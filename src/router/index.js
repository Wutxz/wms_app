import Vue from 'vue';
import VueRouter from 'vue-router';
import { getLocalUser } from '../services/helpers/Auth';
import {
  Default,
  ListStorage,
  ReceiptReport,
  RevealReport,
  CheckReport,
  CarryReport,
  ReportRecPdf,
  ReportRevPdf,
  ReportCheckPdf,
  ReportCarryPdf,
  MaterialReport,
  MaterialReportPdf,
  ListUnit,
  SProduct,
  ListEmployees,
  GetBackReport,
  MinimumReport,
  ReportMinimumPdf,
  
  AddCarryview,
  ReturnReport
} from '../views';

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue'),
  },
  {
    path: '/forgot',
    name: 'Forgot',
    component: () => import('../views/Forgot.vue'),
  },
  {
    path: '/home',
    name: 'Dashboard',
    component: () => import('../views/Dashboard.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/sa/company',
    name: 'CompanyList',
    component: () => import('../views/sa/CompanyList.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/sa/company/wait',
    name: 'CompanyWait',
    component: () => import('../views/sa/CompanyWait.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/sa/company/info',
    name: 'CompanyInfo',
    component: () => import('../views/sa/CompanyInfo.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/ad/user',
    name: 'ListEmployees',
    component: () => import('../views/admin/ListEmployees.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/receive',
    name: 'ReceiveNew',
    component: () => import('../views/user.receive/ReceiveNew.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/receive/old',
    name: 'ReceiveOld',
    component: () => import('../views/user.receive/ReceiveOld.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/ad/material/cate',
    name: 'MaterialCate',
    component: () => import('../views/admin/MaterialCate.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/listStorage',
    name: 'ListStorage',
    component: ListStorage,
  },
  {
    path: '/ad/material/type',
    name: 'MaterialType',
    component: () => import('../views/admin/MaterialType.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/ad/product/cate',
    name: 'ProductCate',
    component: () => import('../views/admin/ProductCate.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/ad/product/type',
    name: 'ProductType',
    component: () => import('../views/admin/ProductType.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/receipt',
    name: 'ReceiptReport',
    component: ReceiptReport,
  },
  {
    path: '/reveal',
    name: 'RevealReport',
    component: RevealReport,
  },
  {
    path: '/check',
    name: 'CheckReport',
    component: CheckReport,
  },
  {
    path: '/carry',
    name: 'CarryReport',
    component: CarryReport,
  },
  {
    path: '/reportrecpdf',
    name: 'ReportRecPdf',
    component: ReportRecPdf,
  },
  {
    path: '/reportrevpdf',
    name: 'ReportRevPdf',
    component: ReportRevPdf,
  },
  {
    path: '/reportcheckpdf',
    name: 'ReportCheckPdf',
    component: ReportCheckPdf,
  },
  {
    path: '/reportcarrypdf',
    name: 'ReportCarryPdf',
    component: ReportCarryPdf,
  },
  {
    path: '/materialreport',
    name: 'MaterialReport',
    component: MaterialReport,
  },
  {
    path: '/materialreportpdf',
    name: 'MaterialReportPdf',
    component: MaterialReportPdf,
  },
  {
    path: '/getbackreport',
    name: 'GetBackReport',
    component: GetBackReport,
  },
  {
    path: '/minimumreport',
    name: 'MinimumReport',
    component: MinimumReport,
  },
  {
    path: '/reportminimumpdf',
    name: 'ReportMinimumPdf',
    component: ReportMinimumPdf,
  },
  {
    path: '/addcarryview',
    name: 'AddCarryview',
    component: AddCarryview,
  },
  {
    path: '/returnreport',
    name: 'ReturnReport',
    component: ReturnReport,
  },
  {
    path: '/Default',
    name: 'Default',
    component: Default,
  },
  {
    path: '/ad/product/goods',
    name: 'Goods',
    component: () => import('../views/admin/Goods.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/ad/material/search',
    name: 'MaterialSearch',
    component: () => import('../views/admin/MaterialSearch.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/check',
    name: 'CheckNew',
    component: () => import('../views/user.check/CheckNew.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/check/return',
    name: 'ReturnNew',
    component: () => import('../views/user.check/ReturnNew.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/order',
    name: 'OrderNew',
    component: () => import('../views/user.order/OrderNew.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/order/list',
    name: 'OrderList',
    component: () => import('../views/user.order/OrderList.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/us/order/report',
    name: 'OrderReport',
    component: () => import('../views/user.order/OrderReport.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/ad/req/order',
    name: 'OrderRequest',
    component: () => import('../views/admin/OrderRequest.vue'),
    meta: { reqAuth: true },
  },
  {
    path: '/listUnit',
    name: 'ListUnit',
    component: ListUnit,
  },
  {
    path: '/sproduct',
    name: 'SProduct',
    component: SProduct,
  },
  {
    path: '/listEmp',
    name: 'ListEmployees',
    component: ListEmployees,
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((route) => route.meta.reqAuth)) {
    let auth = getLocalUser();
    if (auth) {
      next();
    } else {
      next({ path: '/' });
    }
  }
  next();
});

export default router;
